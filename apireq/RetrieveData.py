import requests
import json
import pandas as pd
import os
import arrow
import math

def getData():
    response = requests.get('https://api.coinmarketcap.com/v1/ticker/?limit=0')
    statuscode = response.status_code
    if statuscode != 200:
        return "Nothing retreived"
    response_data = response.json()
    frames = []
    i = 0
    json_string = json.dumps([ob for ob in response_data])
    response_df = pd.read_json(json_string)

    #response_df = response_df.drop(['24h_volume_usd','available_supply','last_updated','market_cap_usd',
                                    #'max_supply','total_supply','price_btc'], axis=1)

    return response_df


def totalTradingVolume(currData):
    coinList = list(currData['id'].unique())
    total_trading_volume = 0
    for coin in coinList:
        maxCoinVolume= currData[(currData['id'] == coin)]['24h_volume_usd'].values[0]
        if not math.isnan(maxCoinVolume):
            total_trading_volume += maxCoinVolume
    return total_trading_volume

def totalMarketCap(currData):
    coinList = list(currData['id'].unique())
    total_market_cap = 0
    for coin in coinList:
        maxCoinMarketCap = currData[(currData['id'] == coin)]['market_cap_usd'].values[0]
        if not math.isnan(maxCoinMarketCap):
            total_market_cap += maxCoinMarketCap
    return total_market_cap

def bitcoinDetails(currData):
    BTCDATA = currData[(currData['symbol'] == "BTC")]
    tradingVolume = BTCDATA['24h_volume_usd'].values[0]
    marketCap = BTCDATA['market_cap_usd'].values[0]
    return tradingVolume, marketCap

def etherDetails(currData):
    ETHERDATA = currData[(currData['symbol'] == "ETH")]
    tradingVolume = ETHERDATA['24h_volume_usd'].values[0]
    marketCap = ETHERDATA['market_cap_usd'].values[0]
    return tradingVolume, marketCap

def writeToCSV(currData):
    pathname = os.path.dirname(__file__) + '/../static/dataset/files/dingdang.csv'
    pathname_ttv = os.path.dirname(__file__) + '/../static/dataset/files/totaltradingvolume.csv'
    pathname_mkc = os.path.dirname(__file__) + '/../static/dataset/files/totalmarketcap.csv'
    pathname_ttv_excel = os.path.dirname(__file__) + '/../static/dataset/files/totaltradingvolume.xlsx'
    pathname_mkc_excel = os.path.dirname(__file__) + '/../static/dataset/files/totalmarketcap.xlsx'
    if not os.path.isfile(pathname):
        log = 1
        curr_time = arrow.now().format()
        total_trading_volume = totalTradingVolume(currData)
        total_market_cap = totalMarketCap(currData)
        btcTV, btcMC = bitcoinDetails(currData)
        ethTV, ethMC = etherDetails(currData)
        print(ethMC)
        print(ethTV)
        othTV = total_trading_volume - btcTV - ethTV
        othMC = total_market_cap - btcMC - btcMC

        '''Total Volume Dataframe'''
        ttvDF = pd.DataFrame(columns=["TotalTTV", "TTVBTC", "TTVETH", "TTVOTH", "log", "time"])
        ttvAr = [total_trading_volume]
        ttvbtcAr = [btcTV]
        ttvethAr = [ethTV]
        ttvothAr = [othTV]
        ttvDF["TotalTTV"] = ttvAr
        ttvDF["TTVBTC"] = ttvbtcAr
        ttvDF["TTVETH"] = ttvethAr
        ttvDF["TTVOTH"] = ttvothAr
        ttvDF["log"] = log
        ttvDF["time"] = curr_time

        '''Market Cap Dataframe'''
        mkcDF = pd.DataFrame(columns=["TotalMKC", "MKCBTC", "MKCETH", "MKCOTH", "log", "time"])
        mkcAr = [total_market_cap]
        mkcbtcAr = [btcMC]
        mkcethAr = [ethMC]
        mkcothAr = [othMC]
        mkcDF["TotalMKC"] = mkcAr
        mkcDF["MKCBTC"] = mkcbtcAr
        mkcDF["MKCETH"] = mkcethAr
        mkcDF["MKCOTH"] = mkcothAr
        mkcDF["log"] = log
        mkcDF["time"] = curr_time

        currData['log'] = log
        currData['time'] = arrow.now()
        currData.to_csv(pathname, index=False)
        ttvDF.to_csv(pathname_ttv, index=False)
        mkcDF.to_csv(pathname_mkc, index=False)
        writeToExcel(pathName=pathname_mkc_excel, dataframe=mkcDF)
        writeToExcel(pathName=pathname_ttv_excel, dataframe=ttvDF)
        return 1
    else:
        curr_time = arrow.now().format()
        existingData = pd.read_csv(pathname)
        existingDataTTV = pd.read_csv(pathname_ttv)
        existingDataMKC = pd.read_csv(pathname_mkc)
        maxIndex = existingData.loc[existingData['log'].idxmax()]
        log = maxIndex['log'] + 1
        #print(maxIndex)
        currData['log'] = log
        #print(currData)
        currData['time'] = curr_time
        toWrite = existingData.append(currData)

        total_trading_volume = totalTradingVolume(currData)
        total_market_cap = totalMarketCap(currData)
        btcTV, btcMC = bitcoinDetails(currData)
        ethTV, ethMC = etherDetails(currData)
        print(ethMC)
        print(ethTV)
        othTV = total_trading_volume - btcTV - ethTV
        othMC = total_market_cap - btcMC - btcMC

        '''Total Volume Dataframe'''
        ttvDF = pd.DataFrame(columns=["TotalTTV", "TTVBTC", "TTVETH", "TTVOTH", "log", "time"])
        ttvAr = [total_trading_volume]
        ttvbtcAr = [btcTV]
        ttvethAr = [ethTV]
        ttvothAr = [othTV]
        ttvDF["TotalTTV"] = ttvAr
        ttvDF["TTVBTC"] = ttvbtcAr
        ttvDF["TTVETH"] = ttvethAr
        ttvDF["TTVOTH"] = ttvothAr
        ttvDF["log"] = log
        ttvDF["time"] = curr_time

        '''Market Cap Dataframe'''
        mkcDF = pd.DataFrame(columns=["TotalMKC", "MKCBTC", "MKCETH", "MKCOTH", "log", "time"])
        mkcAr = [total_market_cap]
        mkcbtcAr = [btcMC]
        mkcethAr = [ethMC]
        mkcothAr = [othMC]
        mkcDF["TotalMKC"] = mkcAr
        mkcDF["MKCBTC"] = mkcbtcAr
        mkcDF["MKCETH"] = mkcethAr
        mkcDF["MKCOTH"] = mkcothAr
        mkcDF["log"] = log
        mkcDF["time"] = curr_time

        toWrite1 = existingDataTTV.append(ttvDF)
        toWrite2 = existingDataMKC.append(mkcDF)
        toWrite.to_csv(pathname, index=False)
        toWrite1.to_csv(pathname_ttv, index=False)
        toWrite2.to_csv(pathname_mkc, index=False)
        writeToExcel(pathName=pathname_mkc_excel, dataframe=toWrite2)
        writeToExcel(pathName=pathname_ttv_excel, dataframe=toWrite1)
        return 2

def retData(minIndex):
    pathname = os.path.dirname(__file__) + '/../static/dataset/files/combined.csv'
    dataset = pd.read_csv(pathname)
    maxIndex = dataset.loc[dataset['log'].idxmax()]['log']
    print(dataset)
    print(maxIndex)
    minIndex = maxIndex - minIndex
    print(minIndex)
    neededData = dataset[(dataset['log'] >= minIndex) & (dataset['log'] <= maxIndex)]
    print(neededData)
    pathname = os.path.dirname(__file__) + '/../static/dataset/files/current.csv'
    pathname_excel = os.path.dirname(__file__) + '/../static/dataset/files/current.xlsx'
    neededData.to_csv(pathname,index=False)
    writeToExcel(pathName=pathname_excel, dataframe=neededData)
    return 1


def newAverageData(minIndex):
    pathname = os.path.dirname(__file__) + '/../static/dataset/files/combined_everything.csv'
    dataset = pd.read_csv(pathname)
    maxIndex = dataset.loc[dataset['log'].idxmax()]['log']
    minIndex = maxIndex - minIndex
    print(minIndex)
    minNeededData = dataset[(dataset['log'] == minIndex)]
    maxNeededData = dataset[(dataset['log'] == maxIndex)]
    print(minNeededData)
    print(maxNeededData)
    coinList = list(minNeededData['id'].unique())
    total_market_cap = totalMarketCap(maxNeededData)
    total_trading_volume = totalTradingVolume(maxNeededData)
    averageList = []

    for coin in coinList:
        minCoinData = minNeededData[(minNeededData['id'] == coin)]
        maxCoinData = maxNeededData[(maxNeededData['id'] == coin)]
        print(maxCoinData)
        symbol = maxCoinData['symbol'].values[0]
        logMaxCost = maxCoinData['price_usd'].values[0]
        logMinCost = minCoinData['price_usd'].values[0]
        averageCost = (logMaxCost - logMinCost)/logMinCost
        #print(maxCoinData['market_cap_usd'].value[0])
        if not math.isnan(maxCoinData['market_cap_usd'].values[0]):
            marketCapPercent = (maxCoinData['market_cap_usd'].values[0] / total_market_cap) * 100
        else:
            marketCapPercent = 0
        if not math.isnan(maxCoinData['24h_volume_usd'].values[0]):
            tradingPercent = (maxCoinData['24h_volume_usd'].values[0] / total_trading_volume) * 100
            tradingVolumeByMarketCap = (maxCoinData['market_cap_usd'].values[0] / total_trading_volume) * 100
        else:
            tradingPercent = 0
            tradingVolumeByMarketCap = 0
        averageCostAr = [averageCost]
        marketCapPercentAr = [marketCapPercent]
        tradingPercentAr = [tradingPercent]
        tradingVolumeByMarketCapAr = [tradingVolumeByMarketCap]
        symbolAr = [symbol]
        coinAr = [coin]
        coinDF = pd.DataFrame(columns=["coin", "symbol", "averageCost", "marketCapPercent", "tradingPercent",
                                       "tradingVolumeByMarketCap"])
        coinDF["coin"] = coinAr
        coinDF["symbol"] = symbolAr
        coinDF["averageCost"] = averageCostAr
        coinDF["marketCapPercent"] = marketCapPercentAr
        coinDF["tradingPercent"] = tradingPercentAr
        coinDF["tradingVolumeByMarketCap"] = tradingVolumeByMarketCapAr
        averageList.append(coinDF)
    averageDF = pd.concat(averageList)
    oppathname = os.path.dirname(__file__) + '/../static/dataset/files/average_new.csv'
    oppathname_excel = os.path.dirname(__file__) + '/../static/dataset/files/average_new.xlsx'
    averageDF.to_csv(oppathname, index=False)
    writeToExcel(pathName=oppathname_excel, dataframe=averageDF)
    return 1

'''
def averageData(minIndex):
    pathname = os.path.dirname(__file__) + '/../static/dataset/files/combined.csv'
    dataset = pd.read_csv(pathname)
    maxIndex = dataset.loc[dataset['log'].idxmax()]['log']
    minIndex = maxIndex - minIndex
    print(minIndex)
    neededData = dataset[(dataset['log'] >= minIndex) & (dataset['log'] <= maxIndex)]
    coinList = list(neededData['id'].unique())
    averageList = []
    for coin in coinList:
        coinData = neededData[(neededData['id'] == coin)]
        #mean = coinData['price_usd'].mean()
        #percentChange1h = coinData['percent_change_1h'].mean()
        symbol = coinData['symbol'].values[0]
        #coinDF = pd.DataFrame(columns=["coin","average_usd","percent_change1h","symbol", "percent_changediff"])
        coinDF = pd.DataFrame(columns=["coin", "symbol", "percent_changediff"])
        coinAr = [coin]
        #avAr = [mean]
        #pchangeAr = [percentChange1h]
        logMaxCost = coinData[(coinData['log'] == maxIndex)]['price_usd'].values[0]
        logMinCost = coinData[(coinData['log'] == minIndex)]['price_usd'].values[0]
        #print(logMinCost)
        perChangeDiff = (logMaxCost - logMinCost) / logMinCost
        print(perChangeDiff)
        coinDF['coin'] = coinAr
        #coinDF['average_usd'] = avAr
        #coinDF['percent_change1h'] = pchangeAr
        coinDF['symbol'] = symbol
        coinDF['percent_changediff'] = [perChangeDiff]
        averageList.append(coinDF)
    averageDF = pd.concat(averageList)
    oppathname = os.path.dirname(__file__) + '/../static/dataset/files/average.csv'
    averageDF.to_csv(oppathname,index=False)
    return 1
'''

def TTVAllDayDetails():
    pathname_ttv = os.path.dirname(__file__) + '/../static/dataset/files/totaltradingvolume.csv'
    to_pathname_ttv = os.path.dirname(__file__) + '/../static/dataset/files/timetotalmtradingvolume1.csv'
    ttvDf = pd.read_csv(pathname_ttv)
    maxIndex = ttvDf.loc[ttvDf['log'].idxmax()]['log']

    tenDE = 24 * 10 * 4
    twentyDE = 24 * 20 * 4
    fiftyDE = 24 * 50 * 4
    hundredDE = 24 * 100 * 4
    twohundredDE = 24 * 200 * 4

    tenDE = maxIndex - tenDE
    twohundredDE = maxIndex - twohundredDE
    twentyDE = maxIndex - twentyDE
    fiftyDE = maxIndex - fiftyDE
    hundredDE = maxIndex - hundredDE

    print(tenDE)
    overallDF = pd.DataFrame(columns=["latestBTCvTTV", "10daysBTCvTTV","20daysBTCvTTV",
                                      "50daysBTCvTTV", "100daysBTCvTTV", "200daysBTCvTTV",
                                      "latestETHvTTV", "10daysETHvTTV", "20daysETHvTTV",
                                      "50daysETHvTTV", "100daysETHvTTV", "200daysETHvTTV",
                                      "latestOTHvTTV", "10daysOTHvTTV", "20daysOTHvTTV",
                                      "50daysOTHvTTV", "100daysOTHvTTV", "200daysOTHvTTV",
                                      ])

    currDF = ttvDf[(ttvDf['log'] == maxIndex)]
    overallDF["latestBTCvTTV"] = [(currDF["TTVBTC"].values[0] / currDF["TotalTTV"].values[0]) * 100]
    currDF = ttvDf[(ttvDf['log'] == tenDE)]
    print(currDF)
    overallDF["10daysBTCvTTV"] = (currDF["TTVBTC"].values[0] / currDF["TotalTTV"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == twentyDE)]
    overallDF["20daysBTCvTTV"] = (currDF["TTVBTC"].values[0] / currDF["TotalTTV"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == fiftyDE)]
    overallDF["50daysBTCvTTV"] = (currDF["TTVBTC"].values[0] / currDF["TotalTTV"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == hundredDE)]
    overallDF["100daysBTCvTTV"] = (currDF["TTVBTC"].values[0] / currDF["TotalTTV"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == twohundredDE)]
    overallDF["200daysBTCvTTV"] = (currDF["TTVBTC"].values[0] / currDF["TotalTTV"].values[0]) * 100

    currDF = ttvDf[(ttvDf['log'] == maxIndex)]
    overallDF["latestETHvTTV"] = (currDF["TTVETH"].values[0] / currDF["TotalTTV"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == tenDE)]
    overallDF["10daysETHvTTV"] = (currDF["TTVETH"].values[0] / currDF["TotalTTV"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == twentyDE)]
    overallDF["20daysETHvTTV"] = (currDF["TTVETH"].values[0] / currDF["TotalTTV"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == fiftyDE)]
    overallDF["50daysETHvTTV"] = (currDF["TTVETH"].values[0] / currDF["TotalTTV"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == hundredDE)]
    overallDF["100daysETHvTTV"] = (currDF["TTVETH"].values[0] / currDF["TotalTTV"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == twohundredDE)]
    overallDF["200daysETHvTTV"] = (currDF["TTVETH"].values[0] / currDF["TotalTTV"].values[0]) * 100


    currDF = ttvDf[(ttvDf['log'] == maxIndex)]
    overallDF["latestOTHvTTV"] = (currDF["TTVOTH"].values[0] / currDF["TotalTTV"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == tenDE)]
    overallDF["10daysOTHvTTV"] = (currDF["TTVOTH"].values[0] / currDF["TotalTTV"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == twentyDE)]
    overallDF["20daysOTHvTTV"] = (currDF["TTVOTH"].values[0] / currDF["TotalTTV"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == fiftyDE)]
    overallDF["50daysOTHvTTV"] = (currDF["TTVOTH"].values[0] / currDF["TotalTTV"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == hundredDE)]
    overallDF["100daysOTHvTTV"] = (currDF["TTVOTH"].values[0] / currDF["TotalTTV"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == twohundredDE)]
    overallDF["200daysOTHvTTV"] = (currDF["TTVOTH"].values[0] / currDF["TotalTTV"].values[0]) * 100

    print(overallDF)
    to_pathname_ttv_excel = os.path.dirname(__file__) + '/../static/dataset/files/timetotalmtradingvolume.xlsx'
    overallDF.to_csv(to_pathname_ttv, index=False)
    writeToExcel(pathName=to_pathname_ttv_excel, dataframe=overallDF)
    return 1

def MKCAllDayDetails():
    pathname_ttv = os.path.dirname(__file__) + '/../static/dataset/files/totalmarketcap.csv'
    to_pathname_ttv = os.path.dirname(__file__) + '/../static/dataset/files/timetotalmarketcap1.csv'
    ttvDf = pd.read_csv(pathname_ttv)
    maxIndex = ttvDf.loc[ttvDf['log'].idxmax()]['log']

    tenDE = 24 * 10 * 4
    twentyDE = 24 * 20 * 4
    fiftyDE = 24 * 50 * 4
    hundredDE = 24 * 100 * 4
    twohundredDE = 24 * 200 * 4
    print(tenDE)

    tenDE = maxIndex - tenDE
    twohundredDE = maxIndex - twohundredDE
    twentyDE = maxIndex - twentyDE
    fiftyDE = maxIndex - fiftyDE
    hundredDE = maxIndex - hundredDE

    overallDF = pd.DataFrame(columns=["latestBTCvMKC", "10daysBTCvMKC","20daysBTCvMKC",
                                      "50daysBTCvMKC", "100daysBTCvMKC", "200daysBTCvMKC",
                                      "latestETHvMKC", "10daysETHvMKC", "20daysETHvMKC",
                                      "50daysETHvMKC", "100daysETHvMKC", "200daysETHvMKC",
                                      "latestOTHvMKC", "10daysOTHvMKC", "20daysOTHvMKC",
                                      "50daysOTHvMKC", "100daysOTHvMKC", "200daysOTHvMKC",
                                      ])

    currDF = ttvDf[(ttvDf['log'] == maxIndex)]
    overallDF["latestBTCvMKC"] = [(currDF["MKCBTC"].values[0] / currDF["TotalMKC"].values[0]) * 100]
    currDF = ttvDf[(ttvDf['log'] == tenDE)]
    print(currDF["TotalMKC"].values[0])
    overallDF["10daysBTCvMKC"] = (currDF["MKCBTC"].values[0] / currDF["TotalMKC"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == twentyDE)]
    overallDF["20daysBTCvMKC"] = (currDF["MKCBTC"].values[0] / currDF["TotalMKC"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == fiftyDE)]
    overallDF["50daysBTCvMKC"] = (currDF["MKCBTC"].values[0] / currDF["TotalMKC"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == hundredDE)]
    overallDF["100daysBTCvMKC"] = (currDF["MKCBTC"].values[0] / currDF["TotalMKC"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == twohundredDE)]
    overallDF["200daysBTCvMKC"] = (currDF["MKCBTC"].values[0] / currDF["TotalMKC"].values[0]) * 100

    currDF = ttvDf[(ttvDf['log'] == maxIndex)]
    overallDF["latestETHvMKC"] = (currDF["MKCETH"].values[0] / currDF["TotalMKC"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == tenDE)]
    overallDF["10daysETHvMKC"] = (currDF["MKCETH"].values[0] / currDF["TotalMKC"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == twentyDE)]
    overallDF["20daysETHvMKC"] = (currDF["MKCETH"].values[0] / currDF["TotalMKC"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == fiftyDE)]
    overallDF["50daysETHvMKC"] = (currDF["MKCETH"].values[0] / currDF["TotalMKC"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == hundredDE)]
    overallDF["100daysETHvMKC"] = (currDF["MKCETH"].values[0] / currDF["TotalMKC"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == twohundredDE)]
    overallDF["200daysETHvMKC"] = (currDF["MKCETH"].values[0] / currDF["TotalMKC"].values[0]) * 100


    currDF = ttvDf[(ttvDf['log'] == maxIndex)]
    overallDF["latestOTHvMKC"] = (currDF["MKCOTH"].values[0] / currDF["TotalMKC"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == tenDE)]
    overallDF["10daysOTHvMKC"] = (currDF["MKCOTH"].values[0] / currDF["TotalMKC"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == twentyDE)]
    overallDF["20daysOTHvMKC"] = (currDF["MKCOTH"].values[0] / currDF["TotalMKC"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == fiftyDE)]
    overallDF["50daysOTHvMKC"] = (currDF["MKCOTH"].values[0] / currDF["TotalMKC"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == hundredDE)]
    overallDF["100daysOTHvMKC"] = (currDF["MKCOTH"].values[0] / currDF["TotalMKC"].values[0]) * 100
    currDF = ttvDf[(ttvDf['log'] == twohundredDE)]
    overallDF["200daysOTHvMKC"] = (currDF["MKCOTH"].values[0] / currDF["TotalMKC"].values[0]) * 100

    print(overallDF)
    to_pathname_ttv_excel = os.path.dirname(__file__) + '/../static/dataset/files/timetotalmarketcap.xlsx'
    overallDF.to_csv(pathname_ttv, index=False)
    writeToExcel(pathName=to_pathname_ttv_excel, dataframe=overallDF)
    return 1

def totalTTVtotalMKC():
    to_pathname_mkc = os.path.dirname(__file__) + '/../static/dataset/files/totalmarketcap.csv'
    to_pathname_ttv = os.path.dirname(__file__) + '/../static/dataset/files/totaltradingvolume.csv'
    ttvDF = pd.read_csv(to_pathname_ttv)
    mkcDF = pd.read_csv(to_pathname_mkc)
    #maxIndex = ttvDF.loc[ttvDF['log'].idxmax()]['log']
    maxIndex = 2

    overallDF = pd.DataFrame(columns=["latestTTVvTotalMarketCap", "10daysTTVvTotalMarketCap","20daysTTVvTotalMarketCap",
                                      "50daysTTVvTotalMarketCap", "100daysTTVvTotalMarketCap", "200daysTTVvTotalMarketCap",
                                      "latestBTCvTotalMarketCap", "10daysBTCvTotalMarketCap", "20daysBTCvTotalMarketCap",
                                      "50daysBTCvTotalMarketCap", "100daysBTCvTotalMarketCap", "200daysBTCvTotalMarketCap",
                                      "latestOTHvTotalMarketCap", "10daysOTHvTotalMarketCap", "20daysOTHvTotalMarketCap",
                                      "50daysOTHvTotalMarketCap", "100daysOTHvTotalMarketCap", "200daysOTHvTotalMarketCap",
                                      "latestETHvTotalMarketCap", "10daysETHvTotalMarketCap", "20daysETHvTotalMarketCap",
                                      "50daysETHvTotalMarketCap", "100daysETHvTotalMarketCap","200daysETHvTotalMarketCap"
                                      ])

    currTTVDF = ttvDF[(ttvDF['log'] == maxIndex)]
    currMKCDF = mkcDF[(mkcDF['log'] == maxIndex)]
    overallDF["latestTTVvTotalMarketCap"] = [currTTVDF['TotalTTV'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]
    overallDF["latestBTCvTotalMarketCap"] = [currTTVDF['TTVBTC'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]
    overallDF["latestETHvTotalMarketCap"] = [currTTVDF['TTVETH'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]
    overallDF["latestOTHvTotalMarketCap"] = [currTTVDF['TTVOTH'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]

    currTTVDF = ttvDF[(ttvDF['log'] == 960)]
    currMKCDF = mkcDF[(mkcDF['log'] == 960)]
    overallDF["10daysTTVvTotalMarketCap"] = [currTTVDF['TotalTTV'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]
    overallDF["10daysBTCvTotalMarketCap"] = [currTTVDF['TTVBTC'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]
    overallDF["10daysETHvTotalMarketCap"] = [currTTVDF['TTVETH'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]
    overallDF["10daysOTHvTotalMarketCap"] = [currTTVDF['TTVOTH'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]

    currTTVDF = ttvDF[(ttvDF['log'] == 1920)]
    currMKCDF = mkcDF[(mkcDF['log'] == 1920)]
    overallDF["20daysTTVvTotalMarketCap"] = [currTTVDF['TotalTTV'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]
    overallDF["20daysBTCvTotalMarketCap"] = [currTTVDF['TTVBTC'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]
    overallDF["20daysETHvTotalMarketCap"] = [currTTVDF['TTVETH'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]
    overallDF["20daysOTHvTotalMarketCap"] = [currTTVDF['TTVOTH'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]

    currTTVDF = ttvDF[(ttvDF['log'] == 4800)]
    currMKCDF = mkcDF[(mkcDF['log'] == 4800)]
    overallDF["50daysTTVvTotalMarketCap"] = [currTTVDF['TotalTTV'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]
    overallDF["50daysBTCvTotalMarketCap"] = [currTTVDF['TTVBTC'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]
    overallDF["50daysETHvTotalMarketCap"] = [currTTVDF['TTVETH'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]
    overallDF["50daysOTHvTotalMarketCap"] = [currTTVDF['TTVOTH'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]

    currTTVDF = ttvDF[(ttvDF['log'] == 9600)]
    currMKCDF = mkcDF[(mkcDF['log'] == 9600)]
    overallDF["100daysTTVvTotalMarketCap"] = [currTTVDF['TotalTTV'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]
    overallDF["100daysBTCvTotalMarketCap"] = [currTTVDF['TTVBTC'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]
    overallDF["100daysETHvTotalMarketCap"] = [currTTVDF['TTVETH'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]
    overallDF["100daysOTHvTotalMarketCap"] = [currTTVDF['TTVOTH'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]

    currTTVDF = ttvDF[(ttvDF['log'] == 19200)]
    currMKCDF = mkcDF[(mkcDF['log'] == 19200)]
    overallDF["200daysTTVvTotalMarketCap"] = [currTTVDF['TotalTTV'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]
    overallDF["200daysBTCvTotalMarketCap"] = [currTTVDF['TTVBTC'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]
    overallDF["200daysETHvTotalMarketCap"] = [currTTVDF['TTVETH'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]
    overallDF["200daysOTHvTotalMarketCap"] = [currTTVDF['TTVOTH'].values[0] / currMKCDF['TotalMKC'].values[0] * 100]

    print(overallDF)
    to_pathname_total = os.path.dirname(__file__) + '/../static/dataset/files/totalCSV.csv'
    to_pathname_total_excel = os.path.dirname(__file__) + '/../static/dataset/files/totalCSV.xlsx'
    overallDF.to_csv(to_pathname_total, index=False)
    writeToExcel(pathName=to_pathname_total_excel, dataframe=overallDF)
    return 1

def lastFormula():
    to_pathname_mkc = os.path.dirname(__file__) + '/../static/dataset/files/timetotalmarketcap.csv'
    to_pathname_ttv = os.path.dirname(__file__) + '/../static/dataset/files/timetotalmtradingvolume.csv'
    ttvDF = pd.read_csv(to_pathname_ttv)
    mkcDF = pd.read_csv(to_pathname_mkc)
    #maxIndex = ttvDF.loc[ttvDF['log'].idxmax()]['log']
    maxIndex = 2
    overallDF = pd.DataFrame(columns=["latestOTHBF", "10daysOTHBF", "20daysOTHBF", "50daysOTHBF",
                                      "100daysOTHBF", "200daysOTHBF"])

    overallDF["latestOTHBF"] = [2 * (ttvDF['latestOTHvTTV'].values[0] - mkcDF['latestOTHvMKC'].values[0]) /
                                (ttvDF['latestOTHvTTV'].values[0] + mkcDF['latestOTHvMKC'].values[0])]

    overallDF["10daysOTHBF"] = [2 * (ttvDF['10daysOTHvTTV'].values[0] - mkcDF['10daysOTHvMKC'].values[0]) /
                                (ttvDF['10daysOTHvTTV'].values[0] + mkcDF['10daysOTHvMKC'].values[0])]

    overallDF["20daysOTHBF"] = [2 * (ttvDF['20daysOTHvTTV'].values[0] - mkcDF['20daysOTHvMKC'].values[0]) /
                                (ttvDF['20daysOTHvTTV'].values[0] + mkcDF['20daysOTHvMKC'].values[0])]

    overallDF["50daysOTHBF"] = [2 * (ttvDF['50daysOTHvTTV'].values[0] - mkcDF['50daysOTHvMKC'].values[0]) /
                                (ttvDF['50daysOTHvTTV'].values[0] + mkcDF['50daysOTHvMKC'].values[0])]

    overallDF["100daysOTHBF"] = [2 * (ttvDF['100daysOTHvTTV'].values[0] - mkcDF['100daysOTHvMKC'].values[0]) /
                           (ttvDF['100daysOTHvTTV'].values[0] + mkcDF['100daysOTHvMKC'].values[0])]

    overallDF["200daysOTHBF"] = [2 * (ttvDF['200daysOTHvTTV'].values[0] - mkcDF['200daysOTHvMKC'].values[0]) /
                           (ttvDF['200daysOTHvTTV'].values[0] + mkcDF['200daysOTHvMKC'].values[0])]

    to_pathname_bf = os.path.dirname(__file__) + '/../static/dataset/files/bigformula.csv'
    to_pathname_bf_excel = os.path.dirname(__file__) + '/../static/dataset/files/bigformula.xlsx'
    writeToExcel(pathName=to_pathname_bf_excel, dataframe=overallDF)
    overallDF.to_csv(to_pathname_bf, index=False)
    return 1

def writeToExcel(pathName, dataframe):
    ExcelWriter = pd.ExcelWriter(pathName)
    dataframe.to_excel(ExcelWriter, 'Sheet 1', index=False)
    return 1
if __name__ == '__main__':
    retData = getData()
    #writeToCSV(retData)
    #retData(3)
    #newAverageData(30)
    #TTVAllDayDetails()
    #MKCAllDayDetails()
    #totalTTVtotalMKC()
    #lastFormula()
