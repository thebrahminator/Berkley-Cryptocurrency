from flask import Flask, url_for, render_template, request, session, flash, send_file, redirect
import os
from apireq import RetrieveData
import threading
from multiprocessing import Process
import pandas as pd
app = Flask(__name__)
app.secret_key="JinjaMasterIsMyNameBiatch"

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLDER = os.path.join(APP_ROOT, "static/")
app.config["UPLOAD_FOLDER"]= UPLOAD_FOLDER

@app.route('/')
def index():
    # fileFD = open('test.txt')
    return render_template('dashboard/dash.html')

@app.route('/downloadCSV', methods=["POST"])
def downloadCSV():
    data = request.form.get('id','10')
    data = int(data)
    data_15min = data * 4
    print("Going to Func")
    RetrieveData.retData(data_15min)
    pathname = os.path.dirname(__file__) + '/static/dataset/files/current.csv'
    return send_file(pathname, as_attachment=True)

@app.route('/downloadAverage')
def downloadAverage():
    pathname = os.path.dirname(__file__) + '/static/dataset/files/average_new.csv'
    return send_file(pathname, as_attachment=True)

@app.route('/checkAllData', methods=["GET"])
def checkAllData():
    minIndex = request.args.get('id', '10')
    minIndex = int(minIndex)
    minIndex = minIndex * 4
    pathname = os.path.dirname(__file__) + '/static/dataset/files/combined.csv'
    dataset = pd.read_csv(pathname)
    maxIndex = dataset.loc[dataset['log'].idxmax()]['log']
    minIndex = maxIndex - minIndex
    print(maxIndex)
    print(minIndex)
    neededData = dataset[(dataset['log'] >= minIndex) & (dataset['log'] <= maxIndex)]
    currentData = neededData.to_dict('records')
    print(currentData)
    return render_template('dashboard/allData.html', neededDatas=currentData)

@app.route('/averageNew')
def averageNew():
    pathname = os.path.dirname(__file__) + '/static/dataset/files/average_new.csv'
    dataset = pd.read_csv(pathname)
    currentData = dataset.to_dict('records')
    print(currentData)
    return render_template('dashboard/allAverage.html', neededDatas=currentData)

@app.route('/averageCSV', methods=["POST"])
def averageCSV():
    data = request.form.get('id', '10')
    data = int(data)
    data_15min = data * 4
    data_15minlist = (data_15min,)
    p = threading.Thread(target=RetrieveData.newAverageData, args=data_15minlist)
    p.start()
    #RetrieveData.averageData(data_15min)
    flash("Press Download after 5 minutes")
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run(debug=True)